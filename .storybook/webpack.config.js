const path = require("path");
const webpack = require("webpack");

const static_url = process.env.STATIC_URL || "http://localhost:9001";

module.exports = {
    plugins: [
        new webpack.DefinePlugin({ 'process.env.STATIC_URL': JSON.stringify(static_url || "/") }),
    ],
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loaders: ['awesome-typescript-loader', 'react-docgen-typescript-loader'],
                include: path.resolve(__dirname, '../src'),
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(woff|woff2|eot|ttf|png|svg|jpg|ico)$/,
                loader: 'file-loader',
                include: path.resolve(__dirname, '../src'),
                exclude: /node_modules/,
                options: {
                    name: '[path][name].[ext]?[hash]',
                    context: 'src'
                }
            }
        ]
    }
}
